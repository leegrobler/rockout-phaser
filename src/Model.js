export default class Model {
  constructor() {
    this._soundOn = true;
    this._musicOn = true;
    this._bgMusicPlaying = false;
    this._pitchDetector = null;
  }

  get soundOn() {
    return this._soundOn;
  }

  set soundOn(value) {
    this._soundOn = value;
  }

  get musicOn() {
    return this._musicOn;
  }

  set musicOn(value) {
    this._musicOn = value;
  }

  get bgMusicPlaying() {
    return this._bgMusicPlaying;
  }

  set bgMusicPlaying(value) {
    this._bgMusicPlaying = value;
  }

  get _pitchDetector() {
    return this.__pitchDetector;
  }

  set _pitchDetector(value) {
    this.__pitchDetector = value;
  }
}
