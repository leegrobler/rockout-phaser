import 'phaser';

export default class Button extends Phaser.GameObjects.Container {
  constructor(scene, x, y, key1, key2, text, toggle, callback) {
    super(scene);
    this.scene = scene;
    this.x = x;
    this.y = y;
    this.key1 = key1;
    this.key2 = key2;

    this.toggleGroup = [];
    this.active = false;
    this.previousClick = null;

    this.button = this.scene.add.sprite(0, 0, this.key1).setInteractive();
    this.text = this.scene.add.text(0, 0, text, { fontSize: '32px', fill: '#fff' });
    Phaser.Display.Align.In.Center(this.text, this.button);

    this.button.name = `${text}Btn`;

    this.add(this.button);
    this.add(this.text);

    this.button.on('pointerdown', function () {
      if(typeof callback === 'string') this.scene.scene.start(callback);
      else {
        if(toggle) this.toggleActive();
        callback();
      }
    }.bind(this));

    this.button.on('pointerover', this.respondPointerOver.bind(this));
    this.button.on('pointerout', this.respondPointerOut.bind(this));

    this.scene.add.existing(this);
  }

  respondPointerOver() { this.button.setTexture(this.key2) }
  respondPointerOut() { this.button.setTexture(this.key1) }

  toggleActive() {
    if(this.active) {
      this.button.on('pointerover', this.respondPointerOver.bind(this));
      this.button.on('pointerout', this.respondPointerOut.bind(this));
      this.button.setTexture(this.key1);
      this.active = false;
    } else {
      this.button.off('pointerover');
      this.button.off('pointerout');
      this.toggleGroup.forEach(btn => { if(btn.active) btn.toggleActive() });
      this.button.setTexture(this.key2);
      this.active = true;
    }
  }

  toggle(visible) {
    this.button.setActive(visible).setVisible(visible);
    this.text.setActive(visible).setVisible(visible);
  }
}