import 'phaser';
import Config from '../Config/config';
import Button from './Button';

export default class GameBoard {
  constructor(scene, exitCallback) {
    this.scene = scene;
    this.exitCallback = exitCallback;
    this.strings = [];
  }

  draw() {
    // CONTROL PANEL
    this.exitBtn = new Button(this.scene, 0, 0, 'blueButton1', 'blueButton2', '✗', false, this.exitCallback);
    this.exitBtn.button.scaleX = 0.25;
    this.exitBtn.x = Config.width - this.exitBtn.button.width * this.exitBtn.button.scaleX * 0.5 - 10;
    this.exitBtn.y = this.exitBtn.button.height * 0.5 + 10;

    // STRINGS
    const colors = [0x42A5F5, 0x76FF03, 0xFFA726];
    this.scene.settings.strings.forEach((v1, i) => {
      const y = 125 + (i * 50);
      const color = v1.active ? colors[i%3] : 0x777777;

      let overlay = null;
      let graphic = this.scene.add.rectangle(60, y, Config.width - 90, i < 3 ? 1 : 2, color).setOrigin(0, 0);
      let label = this.scene.add.text(20, 0, v1.label, { fontSize: 28, color: v1.active ? '#fff' : '#777' });
      label.y = y - label.height / 2;

      if(!v1.active) {
        overlay = this.scene.add.rectangle(10, y, Config.width - 20, 50, 0xffffff, 0.5).setOrigin(0, 0.5);
      }

      this.strings.push({ label, graphic, overlay, active: v1.active });
    });

    // ACTIVATOR
    const edgeOfString = this.strings[0].graphic.width + this.strings[0].graphic.x;
    this.activatorLines = [
      this.scene.add.rectangle(edgeOfString - 110, 75, 1, 350, 0x777777).setOrigin(0, 0),
      this.scene.add.rectangle(edgeOfString - 60, 75, 1, 350, 0x777777).setOrigin(0, 0),
    ];
  }

  toggle(visible) {
    this.strings.forEach(v1 => {
      v1.label.setActive(visible).setVisible(visible);
      v1.graphic.setActive(visible).setVisible(visible);
      if(!!v1.overlay) v1.overlay.setActive(visible).setVisible(visible);
      this.exitBtn.toggle(visible);
    });

    this.activatorLines.forEach(v1 => {
      v1.setActive(visible).setVisible(visible);
    })
  }
}