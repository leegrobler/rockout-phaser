import 'phaser';
import Config from '../Config/config';
import Button from './Button';

export default class GameSettings {
  constructor(scene, playCallback) {

    const baseY = -100;
    const fw = Config.width * 1;
    const hw = Config.width * 0.5;
    const qw = Config.width * 0.25;
    const hh = Config.height * 0.5;
    const qh = Config.height * 0.25;

    this.playCallback = playCallback;
    this.speed = -1;
    this.strings = [
      { label: 'e', active: false },
      { label: 'B', active: false },
      { label: 'G', active: false },
      { label: 'D', active: false },
      { label: 'A', active: false },
      { label: 'E', active: false },
    ];
    this.modifiers = [
      { type: 'natural', active: false },
      { type: 'sharp', active: false },
      { type: 'flat', active: false },
    ];

    // PLAY
    this.playBtn = new Button(scene, hw, 0, 'blueButton1', 'blueButton2', 'Play', false, () => this.play());
    this.playBtn.y = hh + qh;
    
    // DIFFICULTY
    this.diffHeading = scene.add.text(0, hh + baseY - 160, 'Difficulty', { fontSize: 28, align: 'center' });
    this.diffHeading.x = qw - this.diffHeading.width / 2;

    this.diffBtns = [
      new Button(scene, qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'Cake', true, () => this.updateSpeed(3500)),
      new Button(scene, qw, hh + baseY - 20, 'blueButton1', 'blueButton2', 'Easy', true, () => this.updateSpeed(2500)),
      new Button(scene, qw, hh + baseY + 40, 'blueButton1', 'blueButton2', 'Medium', true, () => this.updateSpeed(1500)),
      new Button(scene, qw, hh + baseY + 100, 'blueButton1', 'blueButton2', 'Hard', true, () => this.updateSpeed(1000)),
      new Button(scene, qw, hh + baseY + 160, 'blueButton1', 'blueButton2', 'Ultra', true, () => this.updateSpeed(500))
    ];
    this.diffBtns.forEach(btn => btn.toggleGroup = this.diffBtns);
    this.diffBtns[2].button.emit('pointerdown');

    // STRINGS
    this.strHeading = scene.add.text(0, hh + baseY - 160, 'Strings', { fontSize: 28, align: 'center' });
    this.strHeading.x = fw - qw - this.strHeading.width / 2;

    this.strBtns = [
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'G', true, () => this.updateStrings('G')),
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'B', true, () => this.updateStrings('B')),
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'e', true, () => this.updateStrings('e')),
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'E', true, () => this.updateStrings('E')),
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'A', true, () => this.updateStrings('A')),
      new Button(scene, fw - qw, hh + baseY - 80, 'blueButton1', 'blueButton2', 'D', true, () => this.updateStrings('D')),
    ];
    
    // MODIFIERS
    this.modsHeading = scene.add.text(0, 250, 'Modifiers', { fontSize: 28, align: 'center' });
    this.modsHeading.x = fw - qw - this.modsHeading.width / 2;

    this.modsBtns = [
      new Button(scene, fw - qw, this.modsHeading.y + 80, 'blueButton1', 'blueButton2', '♭', true, () => this.updateMod('flat')),
      new Button(scene, fw - qw, this.modsHeading.y + 80, 'blueButton1', 'blueButton2', '♯', true, () => this.updateMod('sharp')),
      new Button(scene, fw - qw, this.modsHeading.y + 80, 'blueButton1', 'blueButton2', '♮', true, () => this.updateMod('natural')),
    ];

    // STRING & MODIFIER COORDS
    [...this.strBtns, ...this.modsBtns,].forEach((btn, i) => {
      btn.button.emit('pointerdown');
      btn.button.scaleX = 0.25;
      btn.x = btn.x - btn.button.width * btn.button.scaleX * (i < 3 ? i - 1 : i > 5 ? i - 7 : i - 4) * 1.1;
      if(i >=3 && i <= 5) btn.y = btn.y + btn.button.height + 10;
    });
  }

  updateSpeed(speed) {
    if(this.speed === speed) this.speed = -1;
    else this.speed = speed;

    this.togglePlayBtnVisible();
  }

  updateStrings(string) {
    const s = this.strings.find(v1 => v1.label === string);
    s.active = !s.active;

    this.togglePlayBtnVisible();
  }

  updateMod(mod) {
    const m = this.modifiers.find(v1 => v1.type === mod);
    m.active = !m.active;

    this.togglePlayBtnVisible();
  }

  togglePlayBtnVisible() {
    const speed = this.speed > -1
    const strings = this.strings.filter(v1 => !!v1.active).length > 0
    const modifiers = this.modifiers.filter(v1 => !!v1.active).length > 0

    this.playBtn.toggle(speed && strings && modifiers);
  }

  play() {
    this.toggle(false);
    this.playCallback();
  }

  toggle(visible) {
    this.playBtn.toggle(visible);
    this.diffHeading.setActive(visible).setVisible(visible);
    this.modsHeading.setActive(visible).setVisible(visible);
    this.strHeading.setActive(visible).setVisible(visible);
    this.diffBtns.forEach(v1 => v1.toggle(visible));
    this.modsBtns.forEach(v1 => v1.toggle(visible));
    this.strBtns.forEach(v1 => v1.toggle(visible));
  }
}