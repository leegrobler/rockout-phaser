import 'phaser';

const Graphics = Phaser.GameObjects.Graphics;
const Rectangle = Phaser.Geom.Rectangle;

export default class GameUi extends Phaser.GameObjects.Group {
  constructor(scene) {
    super(scene);

    const width = scene.cameras.main.width;
    const height = scene.cameras.main.height;

    let playArea = new Graphics(scene);
    playArea.setName('playArea');
    playArea.lineStyle(4, 0xffffff, 1);
    playArea.strokeRectShape(new Rectangle(10, 10, width - 20, height * 0.5));

    let outputArea = new Graphics(scene);
    outputArea.setName('outputArea');
    outputArea.lineStyle(4, 0xffffff, 1);
    outputArea.strokeRectShape(new Rectangle(10, (height * 0.5) + 20, width - 20, height * 0.25));

    let controlArea = new Graphics(scene);
    controlArea.setName('controlArea');
    controlArea.lineStyle(4, 0xffffff, 1);
    controlArea.strokeRectShape(new Rectangle(10, (height * 0.75) + 30, width - 20, (height * 0.25) - 40));

    this.addMultiple([playArea, outputArea, controlArea], true);
  }
}