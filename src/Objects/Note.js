import 'phaser';
import config from '../Config/config';

export default class Note extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, note) {
    const key = `${note.type}-${note.id.substring(0, 1).toLowerCase()}`;

    super(scene, x, y, key);

    this.scene = scene;
    this.scene.add.existing(this);
    this.scene.events.on('update', this.update, this);

    this.name = key;
    
    this.debugText = this.scene.add.text(x, y + 30, '', { fontSize: 12 });
  }

  update(time, delta) {
    this.x += 2;

    if(this.debugText.scene) {
      this.debugText.text = `x: ${this.x}`;
      this.debugText.x = this.x + 2 - (this.debugText.width / 2);
    }

    if(this.x >= config.width + this.width / 2) {
      this.suicide();
    }
  }

  suicide() {
    console.log('destroying', this.name, this.x);
    this.scene.events.off('update', this.update, this);
    this.debugText.destroy(true);
    this.destroy(true);
  }
}