import * as p5 from 'p5';
import 'p5/lib/addons/p5.sound';
import * as ml5 from 'ml5';

export default class PitchDetector {
  constructor(getAudioContext) {
		this.listening = false;
		this.mic = null;
		this.frequency = null;
    this.startMl5 = null;

		this.initialize(getAudioContext);
  }

	toggleListening() {
		if(this.listening) this.mic.stop();
		else this.mic.start(this.startMl5);

		this.listening = !this.listening;
  }
  
	initialize(getAudioContext) {
		this.gotPitch = (err, frequency) => {
			if(err) console.error('gotPitch:', err);
			else this.frequency = frequency;
			
			if(this.listening) pitch.getPitch(this.gotPitch);
		};
	
		this.startMl5 = () => {
			pitch = ml5.pitchDetection(model_url, audioContext, this.mic.stream, null);
			pitch.getPitch(this.gotPitch);
		};

		let pitch;
		const model_url = 'https://cdn.jsdelivr.net/gh/ml5js/ml5-data-and-models/models/pitch-detection/crepe';
		const audioContext = getAudioContext();
		this.mic = new p5.AudioIn();
	}
}