import 'phaser';
import * as p5 from 'p5';
import 'p5/lib/addons/p5.sound';

import PitchDetector from '../Objects/PitchDetector';

export default class BootScene extends Phaser.Scene {
  constructor () {
    super('Boot');
  }

  preload () {
    this.load.image('logo', 'assets/images/logo.png');
  }

  create () {
    new p5(s => { // initialise p5 & pitch detector
      s.setup = () => { this.sys.game.globals.pitchDetector = new PitchDetector(s.getAudioContext) };
    });

    this.scene.start('Preloader');
  }
};