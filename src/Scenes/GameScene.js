import 'phaser';
import Config from '../Config/config';
import board from '../Objects/GameBoard';
import settings from '../Objects/GameSettings';
import Note from '../Objects/Note';

// this.sys.game.globals.pitchDetector.toggleListening(); // i think this is how you start listening for pitch

export default class GameScene extends Phaser.Scene {
  constructor () {
    super('Game');
  }

  create () {
    // this.notes = [];

    this.settings = new settings(this, () => {
      this.board = new board(this, () => {
        this.endGame();
        this.board.toggle(false);
        this.settings.toggle(true);
      });
      this.board.draw();
      this.startPlaying();
    });

    // this.settings.playBtn.button.emit('pointerdown');

    this.debugText = this.add.text(10, 0, 'Debug:', { fontSize: 12 });
  }

  update() {
    this.displayDebugText();
  }

  displayDebugText() {
    let headingDb = 'Debug';
    let diffDb = `\nDifficulty: ${this.settings.speed}`;
    let strDb = `\nStrings: ${this.settings.strings.map(v1 => `${v1.label}:${v1.active}`).join(', ')}`;
    let modDb = `\nModifiers: ${this.settings.modifiers.map(v1 => `${v1.type}:${v1.active}`).join(', ')}`;
    // let noteDb = `\nNotes: ${this.notes.map(v1 => v1.name).join(', ')}`;

    this.debugText.text = headingDb + diffDb + strDb + modDb;
    // this.debugText.text = headingDb + diffDb + strDb + modDb + noteDb;
    this.debugText.y = Config.height - this.debugText.height - 10;
  }

  startPlaying() {
    const strings = this.board.strings.filter(v1 => v1.active);
    const modifiers = this.settings.modifiers.filter(v1 => v1.active);
    const notes = this.cache.json.get('notes');

    this.noteTimer = this.time.addEvent({
      delay: this.settings.speed,
      loop: true,
      callback: () => {
        const string = strings[(Math.floor(Math.random() * strings.length) + 1) - 1];
        const modifier = modifiers[(Math.floor(Math.random() * modifiers.length) + 1) - 1];
        const availableNotes = notes.filter(v1 => v1.type === modifier.type);
        const note = availableNotes[(Math.floor(Math.random() * availableNotes.length) + 1) - 1];

        new Note(this, string.graphic.x + 25, string.graphic.y, note);
        // const noteObj = new Note(this, string.graphic.x + 25, string.graphic.y, note);
        // noteObj.moving = true;
        // this.notes.push(noteObj);
      },
    });
  }

  endGame() {
    this.noteTimer.remove();
    // this.notes.forEach(v1 => v1.suicide());
    // this.notes = [];
  }
};
