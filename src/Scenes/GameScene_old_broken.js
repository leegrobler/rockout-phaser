// first run " npm i ml5@0.4.3 p5@0.10.2 --save " if you want to restore and use this file

// NPM MODULES
import 'phaser';
// import * as p5 from 'p5';
// import 'p5/lib/addons/p5.sound';
import * as ml5 from 'ml5';

import AudioIn from '../Objects/AudioInput';

// this is how to use p5. see also the p5 related imports at the top of the file
// const sketch = s => {
//   s.preload = () => { console.log('sketch preload...') }
//   s.setup = () => { console.log('sketch setup...') }
//   s.draw = () => { console.log('sketch draw...') }
// }
// let canvas = new p5(sketch);

// this seems to be how to use ml5 (i guess). see also the imports @ top
// console.log('ml5:', ml5.version);

export default class GameScene extends Phaser.Scene {
  constructor () {
    super('Game');

    this.modelUrl = 'https://cdn.jsdelivr.net/gh/ml5js/ml5-data-and-models/models/pitch-detection/crepe';
    this.audioContext = new AudioContext();

    const mediaOptions = {
      audio: {
        sampleRate: this.audioContext.sampleRate,
        echoCancellation: false
      }
    };

    let self = this;
    window.navigator.mediaDevices.getUserMedia(mediaOptions).then(stream => {
      self.stream = stream;
    });

    this.counter = 1000;
  }

  preload () {
    this.load.image('logo', 'assets/logo.png');
  }

  create () {
    // this.add.image(400, 300, 'logo');

    this.pitch = ml5.pitchDetection(this.modelUrl, this.audioContext, this.stream, this.modelLoaded());
  }

  modelLoaded() {
    setTimeout(() => {
      console.log('model has loaded');

      this.pitch.getPitch(this.gotPitch());
    }, 0);

    // setTimeout(() => {}, 0);
  }

  gotPitch(err, freq) {
    if(err) console.error('gotPitch:', err);
    else {
      console.log('gotPitch:', freq);
      if(freq) this.freq = freq;
    }

    if(this.counter > 0) {
      this.counter--;
      this.pitch.getPitch(this.gotPitch());
    }
  }
};
